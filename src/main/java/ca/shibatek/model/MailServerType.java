package ca.shibatek.model;

import lombok.Getter;

public enum MailServerType {
	GMAIL("imap.gmail.com", "993", "imaps");
	
	@Getter
	private String host;
	@Getter
	private String port;
	@Getter
	private String protocol;
	
	MailServerType(final String host, final String port, final String protocol){
		this.host = host;
		this.port = port;
		this.protocol = protocol;
	}
	
	@Override
	public String toString() {
		String value = "MailServer:";
		if(this.equals(GMAIL)) {
			value = value + "protocol= " + GMAIL.getProtocol() + "; host=" + GMAIL.getHost() + "; port=" + GMAIL.getPort();
		}
		
		return value;
	}

}
