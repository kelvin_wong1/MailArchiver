package ca.shibatek.model;

import lombok.Data;

@Data
public class MailServerAccount {

	private String username;
	private String password;
	private String mailServer;
}
