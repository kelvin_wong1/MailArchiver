package ca.shibatek.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MailFolder {

	private String name;
	private int messageCount;
	
}
