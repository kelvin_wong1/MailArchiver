package ca.shibatek.mail.service;

import java.util.List;

import javax.mail.Store;

import ca.shibatek.mail.service.exceptions.MailServiceException;
import ca.shibatek.model.MailFolder;

public interface MailService {
	
	void setStore(Store store);
	void closeConnection();
	boolean isConnected();
	List<MailFolder> getMailFolders() throws MailServiceException;
}
