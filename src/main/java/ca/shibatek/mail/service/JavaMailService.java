package ca.shibatek.mail.service;

import java.util.ArrayList;
import java.util.List;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Store;

import org.springframework.stereotype.Component;

import ca.shibatek.mail.service.exceptions.MailServiceException;
import ca.shibatek.model.MailFolder;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class JavaMailService implements MailService {

	private static int PLACEHOLDER_MESSAGE_COUNT = -1;
	private Store store;
	
	@Override
	public void setStore(Store store) {
		this.store = store;
	}
	
	/**
	 * Returns a List of MailFolder for the Mail account
	 * Folders that throw MessagingException,
	 * a placeholder message count of -1 will be used.
	 * 
	 * return List<MailFolder>
	 * @throws MailServiceException 
	 */
	@Override
	public List<MailFolder> getMailFolders() throws MailServiceException{
		List<MailFolder> mailFolders = new ArrayList<>(); 
		Folder rootFolder; 
		Folder[] folders;

		try { 
			rootFolder = store.getDefaultFolder(); 
			folders = rootFolder.list(); 
		}
		catch(MessagingException e) {
			log.error("Error when attempting to get folders in MailAccount; urlName=" + store.getURLName(), e); 
			throw new MailServiceException("Error retrieving mail folders"); 
		}

		for(Folder folder : folders) { 
			// check if folder can hold messsages, if true
			// create MailFolder object 
			try { 
				if((folder.getType() & Folder.HOLDS_MESSAGES) != 0) { 
					MailFolder mailFolder = getMailFolder(folder.getFullName(), folder.getMessageCount());
					mailFolders.add(mailFolder); 
				} 
			} catch(MessagingException e) {
				log.error("Error getting details for folder. folderName=" + folder.getFullName()); 
				MailFolder mailFolder = getMailFolder(folder.getFullName(), PLACEHOLDER_MESSAGE_COUNT);
				mailFolders.add(mailFolder); 
			} 
		}
		return mailFolders;
	}
	
	private MailFolder getMailFolder(final String folderName, final int messageCount){
		MailFolder mailFolder = new MailFolder();
		mailFolder.setName(folderName);
		mailFolder.setMessageCount(messageCount);
		
		return mailFolder;
	}

	@Override
	public void closeConnection() {
		try {
			store.close();
		} catch (MessagingException e) {
			log.error("Not able to disconnect from mail server. Removing store from service", e);
			this.store = null;
		}
	}

	@Override
	public boolean isConnected() {
		if(store == null) {
			return false;
		}
		return store.isConnected();
	}
}
