package ca.shibatek.mail.store;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.springframework.stereotype.Component;

import ca.shibatek.mail.doa.exceptions.MailServerConnectionException;
import ca.shibatek.model.MailServerAccount;
import ca.shibatek.model.MailServerType;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class MailStoreFactory {
		
	public Store getConnectedMailStore(final MailServerAccount mailServerAccount) throws MailServerConnectionException {
		Store mailStore = null;
		final String mailServerName = mailServerAccount.getMailServer();
		
		MailServerType mailServer = MailServerType.valueOf(mailServerName);
		
		try {
			mailStore = getMailStore(mailServer);
			connectToStore(mailStore, mailServer, mailServerAccount);
		} catch (MessagingException e) {
			log.error("Unable to connect to mail server with account" + mailServer.toString(), e );
			throw new MailServerConnectionException("Cannot connect to mail store:" + mailServerAccount.getUsername() 
			+ ";" + mailServer.toString());		
		}
		
		return mailStore;
	}
	
	private Store getMailStore(MailServerType mailServer) throws NoSuchProviderException{
		Properties connectionProperties = createPropertiesForImapsServer(mailServer);
		Session session = Session.getDefaultInstance(connectionProperties);
		
		Store store;
		store = session.getStore(mailServer.getProtocol());

		return store;
	}
	
	private Properties createPropertiesForImapsServer(MailServerType mailServer) { 
		Properties properties = new Properties(); 
	  
		properties.put("mail.store.protocol", mailServer.getProtocol());
		properties.put("mail.imaps.host", mailServer.getHost());
		properties.put("mail.imaps.port", mailServer.getPort());
	  
		return properties; 
	}
	
	private void connectToStore(Store store, MailServerType mailServer, MailServerAccount mailServerAccount) throws MessagingException {
		store.connect(mailServer.getHost(), mailServerAccount.getUsername(), mailServerAccount.getPassword());
	}
}
