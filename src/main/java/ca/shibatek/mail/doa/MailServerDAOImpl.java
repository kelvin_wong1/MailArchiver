package ca.shibatek.mail.doa;

import java.util.Collections;
import java.util.List;

import javax.mail.Store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.shibatek.mail.doa.exceptions.MailServerConnectionException;
import ca.shibatek.mail.service.MailService;
import ca.shibatek.mail.service.exceptions.MailServiceException;
import ca.shibatek.mail.store.MailStoreFactory;
import ca.shibatek.model.MailFolder;
import ca.shibatek.model.MailServerAccount;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class MailServerDAOImpl implements MailServerDAO {

	private MailService mailService;
	private MailStoreFactory mailStoreFactory;
	
	@Autowired
	public MailServerDAOImpl(MailService mailService, MailStoreFactory mailStoreFactory) {
		this.mailService = mailService;
		this.mailStoreFactory = mailStoreFactory;
	}
	
	@Override
	public void connectToMailServer(MailServerAccount mailServerAccount) {
		log.info("Connecting to mail server with account=" + mailServerAccount.toString());
		Store store = null;
		try {
			store = mailStoreFactory.getConnectedMailStore(mailServerAccount);
		} catch (MailServerConnectionException e) {
			//@TODO
			
		}
		
		mailService.setStore(store);
	}
	
	@Override
	public List<MailFolder> getFolders(){
		log.info("Retriving folders...");
		List<MailFolder> mailFolders = Collections.emptyList();
		
		try {
			mailFolders = mailService.getMailFolders();
		} catch (MailServiceException e) {
			//@TODO

		}
		
		return mailFolders;
	}
	
	@Override
	public void disconnect() {
		mailService.closeConnection();
	}
	
	@Override
	public boolean isConnected() {
		return mailService.isConnected();
	}
}
