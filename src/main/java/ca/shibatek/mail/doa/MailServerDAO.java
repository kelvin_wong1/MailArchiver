package ca.shibatek.mail.doa;

import java.util.List;

import ca.shibatek.model.MailFolder;
import ca.shibatek.model.MailServerAccount;

public interface MailServerDAO {

	void connectToMailServer(MailServerAccount mailServerAccount);
	void disconnect();
	boolean isConnected();
	List<MailFolder> getFolders();
	
}
