package ca.shibatek.mail.doa.exceptions;

public class MailServerConnectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1803715862292923241L;

	public MailServerConnectionException(final String message) {
		super(message);
	}
}
