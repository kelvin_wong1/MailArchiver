package ca.shibatek.controller;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ca.shibatek.model.MailServerAccount;
import ca.shibatek.model.MailServerType;

@Scope(value="session")
@Component(value="accountController")
@ELBeanName(value="accountController")
@Join(path="/accounts", to="/accounts.jsf")
public class AccountController {

	private MailServerAccount account = new MailServerAccount();
	
	public String save() {
		account = new MailServerAccount(); 
		return "/accounts.xhtml?faces-redirect=true";
	}
	
	public MailServerAccount getAccount() {
		return account;
	}
	
	public List<MailServerAccount> getAccounts() {
		return Collections.emptyList();
	}
	
	public List<String> getServerTypes() {
		return Stream.of(MailServerType.values())
					.map(MailServerType::name)
					.collect(Collectors.toList());
	}
	
}
