package ca.shibatek.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ca.shibatek.model.MailFolder;
import ca.shibatek.model.MailServerAccount;
import lombok.Getter;
import lombok.Setter;

@Scope(value="session")
@Component(value="downloadController")
@ELBeanName(value="downloadController")
@Join(path="/download", to="/download.jsf")
public class DownloadController {

	@Getter @Setter
	private MailServerAccount account;

	/**
	 * Downloads the messages for the selected e-mail account
	 */
	public void downloadMessagesForAccount() {
		System.out.println(account);
	}
	
	/**
	 * Returns the folders for the selected e-mail account
	 */
	public List<MailFolder> getFolders(){
		
		if(account == null) {
			return Collections.emptyList();
		}
		
		List<MailFolder> listOfFolders = new ArrayList<>();
		
		MailFolder folder = new MailFolder();
		folder.setName(account.getUsername() + "folder");
		folder.setMessageCount(1);
		
		listOfFolders.add(folder);
		
		return listOfFolders;
	}
	
}
